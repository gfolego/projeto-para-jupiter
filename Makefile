CC=gcc
CFLAGS=-O3 -Wall -pedantic

all: atm

atm: atm.c
	${CC} ${CFLAGS} atm.c -o atm

clean:
	rm -f atm

.PHONY: clean
