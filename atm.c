#include <stdlib.h>
#include <stdio.h>

int setAmount(int* pedido, int* amount, int value) {
    if (pedido == NULL || amount == NULL || value == 0) {
        return 1;
    }

    *amount = *pedido / value;
    *pedido %= value;
    return 0;
}

int main(int argc, char *argv[]){
    if (argc < 2) {
        return 1;
    }
    else {
        int pedido = atoi(argv[1]);
        int hidrogenio, helio, gravidade, gas, oxigenio, atmosfera;

        setAmount(&pedido, &oxigenio, 200);
        setAmount(&pedido, &gas, 50);
        setAmount(&pedido, &atmosfera, 20);
        setAmount(&pedido, &gravidade, 10);
        setAmount(&pedido, &helio, 5);
        setAmount(&pedido, &hidrogenio, 1);

        printf("%d hidrogenio\n", hidrogenio);
        printf("%d helio\n", helio);
        printf("%d gravidade\n", gravidade);
        printf("%d atmosfera\n", atmosfera);
        printf("%d gas\n", gas);
        printf("%d oxigenio\n", oxigenio);

        return 0;
    }
}
